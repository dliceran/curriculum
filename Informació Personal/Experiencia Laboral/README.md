# Experiencia Laboral

### CEIP Concepció Arenal  
![CEIP Concepció Arenal](http://www.xtec.cat/centres/a8002587/)  

He treballat com a Suport Educatiu (2016 - 3 Mesos)

### EAP10 Cap Besòs  
![Cap Besòs](https://atencioprimariaicsbcn.wordpress.com/capbesos/)  

He treballat com a Soci-sanitari (2017 - 3 Mesos)

### Biblioteca Ramon d'Alòs-Moner  
![Biblioteca Ramon d'Alòs-Moner](https://bibliotecavirtual.diba.cat/barcelona-sant-marti-biblioteca-ramon-d-alos-moner)  

He treballat com Auxiliar Administratiu (2017 - 3 Mesos)