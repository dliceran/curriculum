# Currículum 
**Nom:** Dylan Liceran  

**Adreça:** c/Pallars 494, España, Barcelona 08019

**Telèfon:** 617471795

**Mail:** 21753265a@iespoblenou.org

## Actualment  

**Estudiant:** Cursant 2n SMIX  

## Estudis  

**ESO:** A IES Barri Besòs  
2013 - 2017

**CFGM:** A l'Institut Poblenou ECAIB  
2017 - Actualment

## Experiencia Laboral

![Experiencia Laboral](Informació Personal/Experiencia Laboral/README.md)

## Idiomes  

 + Català
 + Espanyol
 + Anglès

## Hobbies  

M'agrada jugar a videojocs i aprendre programació quan tinc temps lliure  

## Capacitats Transversals 

+ Treball en equip
+ Autónom
+ Responsable
+ Treballador

<br> </br>

## Futur i aspiracions  

1. Finalitzar el Cicle Formatiu de Grau Mitjà SMIX
2. Realitzar el Cicle Formatiu de Grau Superior
3. Estudiar Java i C++
4. Treure'm el carnet de conduir
5. Treballar de programador de videojocs  

<br> </br>  

## Dades d'Interés

- Disponibilitat: Tardes de 16:00h a 22:00h
- Sense carnet de conduïr